import React from "react";
import { Input } from "antd";

function Step2(props) {
  return (
    <>
      <div>
        <>
          <br />
          <br />
          <h2>Step 2.</h2>
          <h3>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
            qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
          </h3>
          <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
          <br />
          <br />
          <br />
        </>
        <p>
          <Input
            addonBefore="Input 4"
            name="input4"
            value={props.getState("input4", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 5"
            name="input5"
            value={props.getState("input5", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 6"
            name="input6"
            value={props.getState("input6", "")}
            onChange={props.handleChange}
          />
        </p>
      </div>
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "28%",
          width: "700px",
        }}
      >
        <br />
        <br />
        <h2>Frxnl Blockchanin App.</h2>
        <h3>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
          qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
        </h3>
        <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
        <br />
        <br />
        <br />
      </div>
    </>
  );
}

export default Step2;
