import React from "react";
import { Input } from "antd";

function Step3(props) {
  return (
    <>
      <div>
        <>
          <br />
          <br />
          <h2>Step 3.</h2>
          <h3>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
            qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
          </h3>
          <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
          <br />
          <br />
          <br />
        </>
        <p>
          <Input
            addonBefore="Input 7"
            name="input7"
            value={props.getState("input7", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 8"
            name="input8"
            value={props.getState("input8", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 9"
            name="input9"
            value={props.getState("input9", "")}
            onChange={props.handleChange}
          />
        </p>
      </div>
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "28%",
          width: "700px",
        }}
      >
        <br />
        <br />
        <h2>Frxnl Blockchanin App.</h2>
        <h3>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
          qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
        </h3>
        <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
        <br />
        <br />
        <br />
      </div>
    </>
  );
}

export default Step3;
