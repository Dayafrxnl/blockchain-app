import React from "react";
import { Input } from "antd";

function Step1(props) {
  return (
    <>
      <div>
        <>
          <br />
          <br />
          <h2>Step 1.</h2>
          <h3>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
            qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
          </h3>
          <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
          <br />
          <br />
          <br />
        </>
        <p>
          <Input
            addonBefore="Input 1"
            name="input1"
            value={props.getState("input1", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 2"
            name="input2"
            value={props.getState("input2", "")}
            onChange={props.handleChange}
          />
        </p>
        <p>
          <Input
            addonBefore="Input 3"
            name="input3"
            value={props.getState("input3", "")}
            onChange={props.handleChange}
          />
        </p>
      </div>
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "28%",
          width: "700px",
        }}
      >
        <br />
        <br />
        <h2>Frxnl Blockchanin App.</h2>
        <h3>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
          qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
        </h3>
        <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
        <br />
        <br />
        <br />
      </div>
    </>
  );
}

export default Step1;
