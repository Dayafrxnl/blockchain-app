import React from "react";

function FinalStep(props) {
  return (
    <div>
      <>
        <br />
        <br />
        <h2>Step 3.</h2>
        <h3>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
          qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
        </h3>
        <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
        <br />
        <br />
        <br />
      </>
      <p>
        <b>Input 1:</b> {props.state.input1}
      </p>
      <p>
        <b>Input 2:</b> {props.state.input2}
      </p>
      <p>
        <b>Input 3:</b> {props.state.input3}
      </p>
      <p>
        <b>Input 4:</b> {props.state.input4}
      </p>
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "28%",
          width: "700px",
        }}
      >
        <br />
        <br />
        <h2>Frxnl Blockchanin App.</h2>
        <h3>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Fugit cum
          qui illo sapiente quos ab architecto quasi explicabo, eveniet unde.
        </h3>
        <b>Visit Us:</b> <a href="https://frxnl.com/">Frxnl</a>
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}

export default FinalStep;
