import React from "react";

import { Steps, Step } from "react-step-builder";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import FinalStep from "./FinalStep";
import { Button, Col, Row } from "antd";
import Logo from "./logo.png";

const Navigation = (props) => {
  console.log({ props });
  return (
    <Row align="center">
      {/* <Col>
        <Button type="primary" onClick={props.prev} style={{ marginRight: 10 }}>
          Previous
        </Button>
      </Col> */}
      <Col>
        <Button type="primary" onClick={props.next}>
          Next
        </Button>
      </Col>
    </Row>
  );
};

function App() {
  const config = {
    navigation: {
      component: Navigation, // a React component with special props provided automatically
      location: "after", // or before
    },
  };

  return (
    <div className="App">
      <img
        src={Logo}
        alt="logo"
        style={{
          width: "100px",
          marginTop: "10px",
          position: "absolute",
          left: "10px",
        }}
      />
      <Steps config={config}>
        <Step component={Step1} />
        <Step component={Step2} />
        <Step component={Step3} />
        <Step component={FinalStep} />
      </Steps>
    </div>
  );
}

export default App;
